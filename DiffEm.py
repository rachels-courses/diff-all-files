import subprocess
from subprocess import PIPE, run
import os
from os import listdir
from os.path import isfile, join

def GetSpaces( spaceCount, string ):
    spaces = ""
    length = spaceCount - len( string )
    for i in range( length ):
        spaces += " "
    
    return spaces

def Columned( spaceCount, text ):
    return text + GetSpaces( spaceCount, text )

def PrintAndWrite( text, file ):
    print( text, end="" )
    file.write( text )
    
FILE_COL_SIZE = 75

path = "/home/wilsha/TEACHING/Grading/CS200 for loops"# input( "Enter full path of directory with files to diff: " )

outPath = path + "/diff/Results.txt"
os.system( "mkdir \"" + path + "/diff\"" )

resultFile = open( outPath, "w" )
PrintAndWrite( Columned( 20, "Diff lines" ), resultFile )
PrintAndWrite( Columned( FILE_COL_SIZE, "FileA" ), resultFile )
PrintAndWrite( Columned( FILE_COL_SIZE, "FileB" ), resultFile )
PrintAndWrite( "\n", resultFile )

# https://stackoverflow.com/questions/3207219/how-do-i-list-all-files-of-a-directory
fileList = [f for f in listdir( path ) if isfile(join( path , f))]

for a in range( len( fileList ) ):
    for b in range( a+1, len( fileList ) ):
        fileA = fileList[a]
        fileB = fileList[b]
        
        if fileA == "." or fileA == ".." or fileB == "." or fileB == ".." or fileA == fileB: continue
        
        pathA = path + "/" + fileA
        pathB = path + "/" + fileB
                
        #command = "wdiff -s \"" + pathA + "\" \"" + pathB + "\""# > \"" + outPath + "\""
        command = "sdiff -B -b -s \"" + pathA + "\" \"" + pathB + "\" | wc -l"
        
        # Compare these files
        #https://stackoverflow.com/questions/3503879/assign-output-of-os-system-to-a-variable-and-prevent-it-from-being-displayed-on
        result = subprocess.run( command, stdout=PIPE, stderr=PIPE, universal_newlines=True, shell=True )
        
        PrintAndWrite( Columned( 20, result.stdout.replace( "\n", "" ) ), resultFile )
        PrintAndWrite( Columned( FILE_COL_SIZE, fileA ), resultFile )
        PrintAndWrite( Columned( FILE_COL_SIZE, fileB ), resultFile )
        PrintAndWrite( "\n", resultFile )       
        
        
print( "Outputted result to " + outPath )
resultFile.close()